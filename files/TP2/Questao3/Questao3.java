import java.util.Random;

public class Questao3 {
	
	public static Random gerador;
	
	public static void main(String[] args) {
		String[] entrada = new String[1000];
		int posicao = 0;
		boolean isEOF = false;

		gerador = new Random();
		gerador.setSeed(4);
		do {
			entrada[posicao] = MyIO.readLine();	    
			isEOF = isEOF(entrada[posicao]);
			posicao++;
		} while ( isEOF == false );
		posicao--; // Desconsiderar a ultima linha, por conter "FIM"

		for (int i = 0; i < posicao; i++) {
			String saida = alterarOcorrencia(entrada[i]);
			MyIO.println(saida);
		}
	}

	public static boolean isEOF(String palavra) {
		String flagEOF = "FIM";
		boolean isEOF = true;

		if (palavra.length() == flagEOF.length()) {
			for (int i = 0; i < palavra.length(); i++) {
				if (palavra.charAt(i) != flagEOF.charAt(i)) {
					return false;
				}
			}
		} else {
			isEOF = false;
		}

		return isEOF;
	}

	public static String alterarOcorrencia(String s) {		
		char selectedChar = (char)('a' + (Math.abs(gerador.nextInt()) % 26));
		char toReplace = (char)('a' + (Math.abs(gerador.nextInt()) % 26));

		return alterarOcorrencia(s, 0, selectedChar, toReplace);
	}


	public static String alterarOcorrencia(String s, int i, char selectedChar, char toReplace) {		
		String retorno = "";

		if (i == s.length()) {
			retorno = "";
		} else {		
			char c = s.charAt(i);

			if (c == selectedChar) {
				retorno = toReplace + alterarOcorrencia(s, i + 1, selectedChar, toReplace);
			} else {
				retorno = c + alterarOcorrencia(s, i + 1, selectedChar, toReplace);
			}
		}

		return retorno;
	}	
}
