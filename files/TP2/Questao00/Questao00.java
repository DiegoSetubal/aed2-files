public class Questao00 {
    public static void main(String[] args) {
        String[] entrada = new String[1000];
        int posicao = 0;
        boolean isEOF = false;

        do {
            entrada[posicao] = MyIO.readLine();
            isEOF = isEOF(entrada[posicao]);
            posicao++;
        } while ( isEOF == false );
        posicao--; // Desconsiderar a ultima linha, por conter "FIM"

        for (int i = 0; i < posicao; i++) {
            int qtdMaiusculas = contarMaiusculas(entrada[i]);
            MyIO.println(qtdMaiusculas);
        }
    }

    public static boolean isEOF(String palavra) {
        String flagEOF = "FIM";
        boolean isEOF = true;

        if (palavra.length() == flagEOF.length()) {
            for (int i = 0; i < palavra.length(); i++) {
                if (palavra.charAt(i) != flagEOF.charAt(i)) {
                    return false;
                }
            }
        } else {
            isEOF = false;
        }
        return isEOF;
    }

    public static int contarMaiusculas(String palavra) {
	    return contarMaiusculas(palavra, 0);
    }
    
    public static int contarMaiusculas(String palavra, int i) {
        if (i == palavra.length()) {
            return 0;
        } else if (isUppercase(palavra.charAt(i))) {
            return 1 + contarMaiusculas(palavra, i + 1);
        } else {
            return 0 + contarMaiusculas(palavra, i + 1);
        }
    }

    public static boolean isUppercase(char c) {
	    return ((int) c >= 65 && (int) c <= 90);
    }
}

