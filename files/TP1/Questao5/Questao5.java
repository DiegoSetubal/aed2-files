import java.util.*;

public class Questao5 {
	
	public static void main(String[] args) {
		String[] entrada = new String[1000];
		int posicao = 0;
		boolean isEOF = false;

		do {
			entrada[posicao] = MyIO.readLine();	    
			isEOF = isEOF(entrada[posicao]);
			posicao++;
		} while ( isEOF == false );
		posicao--; // Desconsiderar a ultima linha, por conter "FIM"

		for (int i = 0; i < posicao; i++) {
			boolean isCompostaPorVogais = isCompostaPorVogais(entrada[i]);
			boolean isCompostaPorConsoantes = isCompostaPorConsoantes(entrada[i]);
			boolean isInt = isInteger(entrada[i]);
			boolean isDouble = isDouble(entrada[i]);

			MyIO.print(isCompostaPorVogais ? "SIM " : "NAO ");
			MyIO.print(isCompostaPorConsoantes ? "SIM " : "NAO ");
			MyIO.print(isInt ? "SIM " : "NAO ");
			MyIO.print(isDouble ? "SIM " : "NAO ");
			MyIO.print("\n");		
		}
	}

	public static boolean isEOF(String palavra) {
		String flagEOF = "FIM";
		boolean isEOF = true;

		if (palavra.length() == flagEOF.length()) {
			for (int i = 0; i < palavra.length(); i++) {
				if (palavra.charAt(i) != flagEOF.charAt(i)) {
					return false;
				}
			}
		} else {
			isEOF = false;
		}

		return isEOF;
	}
	
	public static boolean isCompostaPorVogais(String s) {
		
		boolean isComposta = true;
		String toLowercase = toLowercase(s);

		for (int i = 0; i < toLowercase.length(); i++) {
			int c = (int) toLowercase.charAt(i);
			
			if (c == 97 || c == 101 || c == 105 || c == 111 || c == 117) {
				isComposta = true;
			} else {
				isComposta = false;
				i = toLowercase.length();
			}
		}

		return isComposta;
	}

	public static boolean isCompostaPorConsoantes(String s) {

		boolean isComposta = true;
		//String toLowercase = toLowercase(s);

		for (int i = 0; i < s.length(); i++) {
			int c = (int) s.charAt(i);
			
			if (c == 97 || c == 101 || c == 105 || c == 111 || c == 117 || c == (97 - 32) || c == (101 - 32) || c == (105 - 32) || c == (111 - 32) || c == (117 - 32)) {
				isComposta = false;
				i = s.length();
			}
		}

		return isComposta;
	}
		
	public static boolean isInteger(String s) {
		try {
			int i = Integer.parseInt(s);
		} catch (NumberFormatException e) {
			return false;
		}

		return true;
	}

	public static boolean isDouble(String s) {
		try {
			double d = Double.parseDouble(s);
		} catch (NumberFormatException e) {
			return false;
		}
	
		return true;
	}

	public static String toLowercase(String s) {
		String saida = "";

		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			
			if (c >= 65 && c <= 90) {
				saida += (char)(c + 32);
			} else {
				saida += c;
			}
		}

		return saida;
	}

}







