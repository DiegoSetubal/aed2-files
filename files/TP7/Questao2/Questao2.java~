/**
 * Classe árvore binária de municipios.
 * @author Diego Oliveira
 */
class No1 {
	private int codigoUF;
	public No1 esq, dir;
	public No2 raizArvore2;

	No1(int codigoUF) {
		this(null, null, null, codigoUF);
	}

	No1(No1 esq, No1 dir, No2 raizArvore2, int codigoUF) {
		this.esq = esq;
		this.dir = dir;
		this.setCodigoUF(codigoUF);
	}

	public void setCodigoUF(int codigoUF) {
		this.codigoUF = codigoUF;
	}

	public int getCodigoUF() {
		return this.codigoUF;
	}
}

class No2 {
	private Municipio municipio;
	public No2 esq, dir;

	No2(Municipio m) {		
		this(null, null, m);
	}
	
	No2(No2 esq, No2 dir, Municipio m) {
		this.esq = esq;
		this.dir = dir;
		this.setMunicipio(m);
	}

	public void setMunicipio(Municipio m) {
		this.municipio = m.clone();
	}

	public Municipio getMunicipio() {
		return this.municipio;
	}
}


class Arvore {
	private No raiz;
	private int numComparacoes;


	/**
	 * Construtor da classe.
	 */
	public Arvore() {
		this.raiz = null;
		this.setNumComparacoes(0);
	}

	public void setNumComparacoes(int x) {
		this.numComparacoes = x;
	}

	public int getNumComparacoes() {
		return this.numComparacoes;
	}
	
	/**
	 * Insere um elemento na arvore 1.
	 */
	public void inserirA1(int codigoUF) {
		raiz = inserirA1(codigoUF, raiz);
	}
	
	/**
	 * Método privado para inserir na arvore 1.
	 * @param codigoUF codigoUF a ser inserido.
	 * @param i No da arvore 1 a ser verificado.
	 * @return i No verificado, alterado ou não.
	 */
	private No1 inserirA1(int codigoUF, No1 raiz) {
		if (i == null) {
			i = new No1(codigoUF);
		} else if (codigoUF < i.getCodigoUF()) {
			i.esq = inserirArvore1(codigoUF, i.esq);
		} else if (codigoUF > i.getCodigoUF()) {
			i.dir = inserirArvore1(codigoUF, i.dir);
		}

		return i;
	}

	/**
	 * Pesquisa a existencia de um elemento na arvore 1.
	 * @param codigoUF codigo a ser pesquisado.
	 * @return <code>true</code> se o elemento existir, <code>false</code> caso contrário.
	 */
	public boolean pesquisar(int id) {
		MyIO.print("raiz ");
		return pesquisar(id, raiz);
	}

	/**
	 * Método privado para pesquisa.
	 * @param id ID a ser pesquisado nas árvores.
	 * @param i No da árvore a ser analisado.
	 * @return <code>true</code> se existir um municipio com o id, <code>false</code> caso contrário.
	 */

	private boolean pesquisar(int id, No1 i) {
	 	boolean resp = false;
		
		if (i != null) {
			MyIO.print("esq ");
			resp = this.pesquisar(id, i.esq);
			boolean respA2 = this.pesquisarA2(id, i.raizArvore2);
			if (respA2 == false) {
				resp = this.pesquisar(id, i.dir);
			} else {
				resp = true;
			}
		}

		return resp;
	}
	
	/**
	 * Método para pesquisa de Id na segunda árvore.
	 * @param id Elemento a ser pesquisado.
	 * @param i No a ser analisado.
	 * @return <code>true</code> se existir municipio com o id selecionado, <code>false</code> caso contrário.
	 */
	private boolean pesquisarA2(int id, No2 i) {	
		boolean resp = false;
		
		if (i == null) {
			resp = false;
		} else if (i.getMunicipio().getID() == id) {
			resp = true;
		} else if (id < i.getMunicipio().getID()) {
			resp = pesquisarA2(id, i.esq);
		} else if (id > i.getMunicipio().getID()) {
			resp = pesquisarA2(id, i.dir);
		}

		return resp;
	}

	public void inserir(Municipio m) {
		raiz.raizArvore2 = inserir(m, raiz, raiz.raizArvore2);
	}
	
	/**
	 * Insere um elemento na arvore 2.
	 * @param m Municipio a ser inserido.
	 * @param i No da primeira arvore a ser analisado
	 * @param i No da segunda arvore.
	 * @return j No da segunda arvore.
	 */
	public No2 inserir(Municipio m, No1 i, No2 j) throws Exception {
		if (m.getCodigoUF() == i.getCodigoUF()) {
			j = this.inserirA2(m, j);
		} else if (m.getCodigoUF() < i.getCodigoUF()) {
			j = this.inserir(m, i.esq, j);
		} else if (m.getCodigoUF() > i.getCodigoUF()) {
			j = this.inserir(m, i.dir, j);
		}

		return j;
	}

	/**
	 * Método privado para inserir um elemento.
	 * @param m Municipio a ser inserido.
	 * @param i No a ser verificado para inserção.
	 * @return i No verificado, alterado ou não.
	 * @throws Exception Se o elemento já existir.
	 */
	private No2 inserirA2(Municipio m, No2 i) throws Exception {
		if (i == null) {
			i = new No(m.clone());
		} else if (m.getID() < i.getMunicipio().getID()) {
			i.esq = this.inserir(m, i.esq);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (m.getID() > i.getMunicipio().getID()) {
			i.dir = this.inserir(m, i.dir);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else {
			//throw new Exception("Erro ao inserir: elemento já existe");
		}

		return i;
	}
	
	/**
	 * Método iterativo para remoção de municipio.
	 * @param id A chave de remoção do elemento.
	 */
	public void remover(int id) throws Exception {
		raiz.raizArvore2 = remover(id, raiz, raiz.raizArvore2);
	}

	private No2 remover(int id, No1 i, No2 j) {
		if (i == null) {
			//
		} if (pesquisarParaRemover(id, i)) {
			j = this.removerA2(id, j);
		} else {
			if (this.pesquisarParaRemover(id, i.esq) {
				j = this.remover(id, i.esq, j);
			} else if (this.pesquisarParaRemover(id, i.dir) {
				j = this.remover(id, i.dir, j);
			}			
		}

		return j;
	}

	/**
	 * Método privado para remoção do municipio
	 * @param id A chave de pesquisa da remoção.
	 * @param i No em análise.
	 * @return No verificado, modificado ou não.
	 */
	private No2 removerA2(int id, No2 i) throws Exception {
		if (i == null) {
			//throw new Exception("Erro ao remover");
		} else if (id < i.getMunicipio().getID()) {
			i.esq = this.remover(id, i.esq);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if  (id > i.getMunicipio().getID()) {
			i.dir = this.remover(id, i.dir);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (i.dir == null) {
			i = i.esq;
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (i.esq == null) {
			i = i.dir;
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else {
			i.esq = this.antecessor(i, i.esq);
		}
		
		return i;
	}

	private boolean pesquisarParaRemover(int id, No1 i) {
	 	boolean resp = false;
		
		if (i != null) {
			resp = this.pesquisar(id, i.esq);
			boolean respA2 = this.pesquisarA2(id, i.raizArvore2);
			if (respA2 == false) {
				resp = this.pesquisar(id, i.dir);
			} else {
				resp = true;
			}
		}

		return resp;
	}

	private boolean pesquisarA2(int id, No2 i) {	
		boolean resp = false;
		
		if (i == null) {
			resp = false;
		} else if (i.getMunicipio().getID() == id) {
			resp = true;
		} else if (id < i.getMunicipio().getID()) {
			resp = pesquisarA2(id, i.esq);
		} else if (id > i.getMunicipio().getID()) {
			resp = pesquisarA2(id, i.dir);
		}

		return resp;
	}

	private No antecessor(No2 i, No2 j) {
		if (j.dir != null) {
			j.dir = antecessor(i, j.dir);
		} else {
			i.setMunicipio(j.getMunicipio());
			j = j.esq;
		}

		return j;
	}

	public void mostrar() {
		this.mostrar(raiz);
	}

	private void mostrar(No i) {
		if (i != null) {
			this.mostrar(i.esq);
			i.getMunicipio().imprimir();
			this.mostrar(i.dir);
		}
	}
}

class Municipio {

	//Atributos
	private int id;
	private String nome;
	private String UF;
	private int codigoUF;
	private int populacao;
	private int numFuncionarios;
	private int numComissionados;
	private String escolaridade;
	private String estagio;
	private int attPlano;
	private String regiao;
	private int attCadastro;
	private boolean isConsorcio;

	Municipio() {
		this(0, "", "", 0, 0, 0, 0, "", "", 0, "", 0, false);
	}

	Municipio(int id, String nome, String UF, int codigoUF, int populacao, int numFuncionarios, int numComissionados, String escolaridade, String estagio, int attPlano, String regiao, int attCadastro, boolean isConsorcio) {

		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);		
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setIsConsorcio(isConsorcio);
	}

	//Getters e Setters

	public void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return this.id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}

	public void setUF(String UF) {
		this.UF = UF;
	}

	public String getUF() {
		return this.UF;
	}

	public void setCodigoUF(int codigoUF) {
		this.codigoUF = codigoUF;
	}

	public int getCodigoUF() {
		return this.codigoUF;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public int getPopulacao() {
		return this.populacao;
	}

	public void setNumFuncionarios(int numFuncionarios) {
		this.numFuncionarios = numFuncionarios;
	}

	public int getNumFuncionarios() {
		return this.numFuncionarios;
	}

	public void setNumComissionados(int numComissionados) {
		this.numComissionados = numComissionados;
	}

	public int getNumComissionados() {
		return this.numComissionados;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getEscolaridade() {
		return this.escolaridade;
	}

	public void setEstagio(String estagio) {
		this.estagio = estagio;
	}

	public String getEstagio() {
		return this.estagio;
	}

	public void setAttPlano(int attPlano) {
		this.attPlano = attPlano;
	}

	public int getAttPlano() {
		return this.attPlano;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getRegiao() {
		return this.regiao;
	}

	public void setAttCadastro(int attCadastro) {
		this.attCadastro = attCadastro;
	}

	public int getAttCadastro() {
		return this.attCadastro;
	}

	public void setIsConsorcio(boolean isConsorcio) {
		this.isConsorcio = isConsorcio;
	}

	public boolean getIsConsorcio() {
		return this.isConsorcio;
	}

	public Municipio clone() {
		Municipio clone = new Municipio();

		clone.setID(this.getID());
		clone.setNome(this.getNome());
		clone.setUF(this.getUF());
		clone.setCodigoUF(this.getCodigoUF());
		clone.setPopulacao(this.getPopulacao());
		clone.setNumFuncionarios(this.getNumFuncionarios());
		clone.setNumComissionados(this.getNumComissionados());
		clone.setEscolaridade(this.getEscolaridade());
		clone.setEstagio(this.getEstagio());
		clone.setAttPlano(this.getAttPlano());
		clone.setRegiao(this.getRegiao());
		clone.setAttCadastro(this.getAttCadastro());
		clone.setIsConsorcio(this.getIsConsorcio());

		return clone;
	}

	public void imprimir() {
		MyIO.println(
			this.getID() + " " +
			this.getNome() + " " +
			this.getUF() + " " +
			this.getCodigoUF() + " " +
			this.getPopulacao() + " " +
			this.getNumFuncionarios() + " " +
			this.getNumComissionados() + " " +
			this.getEscolaridade() + " " +
			this.getEstagio() + " " +
			this.getAttPlano() + " " +
			this.getRegiao() +  " " +
			this.getAttCadastro() + " " +
			this.getIsConsorcio() + ""
		);
	}

	public void ler(int registro) {
		String charset = "ISO-8859-1"; //ISO-8859-1
		int i = 0;
		String linha;

		//Arquivo 1
		String arq = "/tmp/articulacaoointerinstitucional.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq1Data = linha.split("\t");

		//Variáveis a serem extraídas do arquivo
		int id = (this.isInteger(arq1Data[0])) ? Integer.parseInt(arq1Data[0]) : 0;
		int codigoUF = Integer.parseInt(arq1Data[1]);
		String nome = arq1Data[3];

		/////////////DEBUGAR ESTA JOÇA!

		boolean isConsorcio = (arq1Data[5].equals("Sim")) ? true : false;

		Arq.close();

		//Arquivo 2
		arq = "/tmp/gestaoambiental.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq2Data = linha.split("\t");
		String estagio = arq2Data[7];

		Arq.close();

		//Arquivo 3
		arq = "/tmp/planejamentourbano.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq3Data = linha.split("\t");

		String escolaridade = arq3Data[5];
		int attPlano = (this.isInteger(arq3Data[8])) ? Integer.parseInt(arq3Data[8]) : 0;

		Arq.close();

		//Arquivo 4
		arq = "/tmp/recursoshumanos.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq4Data = linha.split("\t");

		int numFuncionarios = this.isInteger(arq4Data[7]) ? Integer.parseInt(arq4Data[4]) : 0;
		int numComissionados = (this.isInteger(arq4Data[7])) ? Integer.parseInt(arq4Data[7]) : 0;

		Arq.close();

		//Arquivo 5
		arq = "/tmp/recursosparagestao.txt";
		Arq.openRead(arq, charset);
		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq5Data = linha.split("\t");

		int attCadastro = this.isInteger(arq5Data[6]) ? Integer.parseInt(arq5Data[6]) : 0;

		Arq.close();

		//Arquivo 6
		arq = "/tmp/terceirizacaoeinformatizacao.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq6Data = linha.split("\t");

		Arq.close();

		//Arquivo 7
		arq = "/tmp/variaveisexternas.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq7Data = linha.split("\t");

		String regiao = arq7Data[1];
		int populacao = this.isInteger(arq7Data[6]) ? Integer.parseInt(arq7Data[6]) : 0;
		String UF = arq7Data[3];

		Arq.close();

		//Setando os atributos
		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setRegiao(regiao);
		this.setAttCadastro(attCadastro);
		this.setIsConsorcio(isConsorcio);
	}

	public boolean isInteger(String p) {
		boolean isInt = false;
		try {
			int number = Integer.parseInt(p);

			isInt = true;
		} catch (Exception e) {
			isInt = false;
		}

		return isInt;
	}

	public boolean equals(String s1, String s2) {
		boolean equals = false;		
		
		if (s1.length() == s2.length()) {
			for (int i = 0; i < s1.length(); i++) {
				MyIO.println(s1.charAt(i) + " " + s2.charAt(i));
			}
		}

		return equals;
	}
}

public class Questao1 {
	public static void main(String[] args) throws Exception {

		Arvore ab = new Arvore();
		MyIO.setCharset("ISO-8859-1");

		for (int linha = MyIO.readInt(); linha != 0; linha = MyIO.readInt()) {
			Municipio m = new Municipio();
			m.ler(linha);
			ab.inserir(m);
		}

		int contador = MyIO.readInt();
		
		long inicio = System.currentTimeMillis();
			
		for (int i = 0; i < contador; i++) {
			String comando = MyIO.readString();
			Municipio mu = new Municipio();

			if (comando.charAt(0) == 'I') {
				int registro = MyIO.readInt();
				mu.ler(registro);
				ab.inserir(mu);
			} else if (comando.charAt(0) == 'R') {
				int id = MyIO.readInt();
				ab.remover(id);
			}
		}
		
		for (String linha1 = MyIO.readLine(); linha1.equals("FIM") == false; linha1 = MyIO.readLine()) {
			int id = Integer.parseInt(linha1);

			boolean exists = ab.pesquisar(id);
			String saida = exists ? "SIM" : "NAO";
			MyIO.println(saida);
		}

		long fim = System.currentTimeMillis();
		long diff = (fim - inicio) / 1000;

		writeLog(diff, ab.getNumComparacoes());
	}

	public static void writeLog(long time, int comparacoes) {
		Arq.openWrite("matricula_arvoreBinaria.txt");
		Arq.print("561325\t");
		Arq.print(time + "\t" + comparacoes);
		Arq.close();
	}
}
