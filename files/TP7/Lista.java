/**
 * Classe lista com alocação dinâmica, com celulas.
 * @author Diego Oliveira
 */
class Celula {
	private Municipio municipio;
	public Celula prox;

	Celula() {
		this.prox = null;
		this.setMunicipio(new Municipio());
	}
	
	Celula(Municipio m) {
		this.prox = null;
		this.setMunicipio(m);
	}

	private void setMunicipio(Municipio m) {
		this.municipio = m.clone();
	}

	public Municipio getMunicipio() {
		return this.municipio;
	}
}

class Lista {
	private Celula primeiro;
	private Celula ultimo;

	/**
	 * Construtor da classe.
	 */
	public Lista() {
		this.primeiro = new Celula();
		this.ultimo = primeiro;
	}
	
	/**
	 * Insere um elemento no início da lista.
	 * @param m municipio a ser inserido na lista.
	 */
	public void inserirInicio(Municipio m) {
		Celula tmp = new Celula(m);
		tmp.prox = primeiro.prox;
		primeiro.prox = tmp;
		
		if (primeiro == ultimo) {
			ultimo = tmp;
		}
		
		tmp = null;
	}

	/**
	 * Insere um elemento no final da lista.
	 * @param m municipio a ser inserido na lista.
	 */
	public void inserirFim(Municipio m) {
		ultimo.prox = new Celula(m);
		ultimo = ultimo.prox;
	}
	
	/**
	 * Remove um elemento do começo da lista.
	 * @return Municipio a ser removido da lista.
	 * @throws Exception se a lista estiver vazia.
	 */
	public Municipio removerInicio() throws Exception {
		if (this.isVazia()) {
			throw new Exception("Erro ao remover.");
		}
		
		Celula tmp = primeiro;
		primeiro = primeiro.prox;

		Municipio resp = primeiro.getMunicipio().clone();
		tmp.prox = null;
		tmp = null;

		return resp;
	}

	/**
	 * Remove um elemento do final da lista.
	 * @return Municipio a ser removido.
	 * @throws Exception se a lista estiver vazia.
	 */
	public Municipio removerFim() throws Exception {
		if (this.isVazia()) {
			throw new Exception("Erro ao remover.");
		}

		Celula i;
		for (i = primeiro; i.prox != ultimo; i = i.prox);

		Municipio resp = ultimo.getMunicipio().clone();
		ultimo = i;
		i = ultimo.prox = null;

		return resp;
	}

	/**
     * Insere um elemento em uma posicao especifica considerando que o 
     * primeiro elemento valido esta na posicao 0.
     * @param m Municipio a ser inserido.
     * @param pos int posição onde o elemento será  inserido.
     * @throws Exception Se <code>pos</code> invalida.
     */
	public void inserir(Municipio m, int pos) throws Exception {
		
		int tamanho = this.getTamanho();

		if (pos < 0 || pos > tamanho) {
			throw new Exception("Posição inválida.");
		} else if (pos == 0) {
			this.inserirInicio(m);
		} else if (pos == tamanho) {
			this.inserirFim(m);
		} else {
			Celula i = primeiro;
			for (int j = 0; j < pos; j++, i = i.prox);

			Celula tmp = new Celula(m);
			tmp.prox = i.prox;
			i.prox = tmp;
			tmp = i = null;
		}
	}

	/**
     * Remove um elemento de uma posição específica da lista
	 * considerando que a primeira posição válida é 0.
     * @param pos Posição a ser removida
     * @return resp int elemento a ser removido.
     * @throws Exception Se <code>posicao</code> invalida.
     */
	public Municipio remover(int pos) throws Exception {
		Municipio resp;
		int tamanho = this.getTamanho();

		if (this.isVazia()){
			throw new Exception("Erro ao remover (vazia)!");
		} else if(pos < 0 || pos >= tamanho){
			throw new Exception("Erro ao remover (posicao " + pos + " / " + tamanho + " invalida!");
		} else if (pos == 0){
			resp = this.removerInicio();
		} else if (pos == tamanho - 1){
			resp = this.removerFim();
		} else {
			Celula i = primeiro;
			for(int j = 0; j < pos; j++, i = i.prox);

			Celula tmp = i.prox;
			resp = tmp.getMunicipio().clone();
			i.prox = tmp.prox;
			tmp.prox = null;
			i = tmp = null;
		}

		return resp;
	}

	/**
	 * Verifica a existência de um municipio com o id indicado por parâmetro.
	 * @param id ID do município a ser pesquisado.
	 * @return <code>true</code> se o municipio existir, <code>false</code> caso contrário.
	 */
	public boolean pesquisar(int id) {
		boolean exists = false;

		for (Celula i = primeiro.prox; i != null && (exists == false); i = i.prox) {
			Municipio m = i.getMunicipio();
			exists = (m.getID() == id);
		}

		return exists;
	}

	public void mostrar() {
		for (Celula i = primeiro.prox; i != null; i = i.prox) {
			i.getMunicipio().imprimir();
		}
	}

	public int getTamanho() {
		int tamanho = 0;
		for (Celula tmp = primeiro; tmp != ultimo; tmp = tmp.prox, tamanho++);
		
		return tamanho;
	}

	public boolean isVazia() {
		return (primeiro == ultimo);
	}
	
}
