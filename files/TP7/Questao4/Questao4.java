/**
 * Classe arvore alvinegra de municipios.
 * @author Diego Oliveira
 */
class No {
	public Municipio municipio;
	public No esq, dir;
	public boolean cor;

	public No() {
		this(new Municipio());
	}

	No(Municipio m) {		
		this(null, null, m, false);
	}

	No(Municipio m, boolean cor) {
		this(null, null, m, cor);
	}
	
	No(No esq, No dir, Municipio m, boolean cor) {
		this.cor = cor;
		this.esq = esq;
		this.dir = dir;
		this.setMunicipio(m);
	}

	public void setMunicipio(Municipio m) {
		this.municipio = m.clone();
	}

	public Municipio getMunicipio() {
		return this.municipio;
	}
}

class ArvoreAlvinegra {
	private No raiz;
	private int numComparacoes;


	/**
	 * Construtor da classe.
	 */
	public ArvoreAlvinegra() {
		this.raiz = null;
		this.setNumComparacoes(0);
	}

	public void setNumComparacoes(int x) {
		this.numComparacoes = x;
	}

	public int getNumComparacoes() {
		return this.numComparacoes;
	}
	
	/**
	 * Metodo para inserir um elemento.
	 * @param m Municipio a ser inserido.
	 * @throws Exception Se o elemento ja existir.
	 */
	public void inserir(Municipio m) throws Exception {
		if (raiz == null) {
			raiz = new No(m, false);
		} else if (raiz.esq == null && raiz.dir == null) {
			if (raiz.municipio.getID() > m.getID()) {
				raiz.esq = new No(m, true);
			} else {
				raiz.dir = new No(m, true);
			}
		} else if (raiz.esq == null) {
			if (raiz.municipio.getID() > m.getID()) {
				raiz.esq = new No(m);
			} else if (raiz.dir.municipio.getID() > m.getID()) {
				raiz.esq = new No(raiz.municipio);
				raiz.municipio = m.clone();
			} else {
				raiz.esq = new No(raiz.municipio);
				raiz.municipio = raiz.dir.municipio;
				raiz.dir.municipio = m.clone();
			}

			raiz.esq.cor = raiz.dir.cor = false;
		} else if (raiz.dir == null) {
			if (raiz.municipio.getID() < m.getID()) {
				raiz.dir = new No(m);
			} else if (raiz.esq.municipio.getID() < m.getID()) {
				raiz.dir = new No(raiz.municipio);
				raiz.municipio = m;
			} else {
				raiz.dir = new No(raiz.municipio);
				raiz.setMunicipio(raiz.esq.municipio);
				raiz.esq.municipio = m;
			}

			raiz.esq.cor = raiz.dir.cor = false;
		} else {
			this.inserir(m, null, null, null, raiz);
		}

		raiz.cor = false;
	}

	private void inserir(Municipio m, No bisavo, No avo, No pai, No i) {
		if (i == null) {
			if (m.getID() < pai.municipio.getID()) {
				i = pai.esq = new No(m, true);
			} else {
				i = pai.dir = new No(m, true);
			}

			if (pai.cor == true) {
				this.balancear(bisavo, avo, pai, i);
			}
		} else {
			if (i.esq != null && i.dir != null && i.esq.cor == true && i.dir.cor == true) {
				i.cor = true;
				i.esq.cor = i.dir.cor = false;

				if (i == raiz) {
					i.cor = false;
				} else if (pai.cor == true) {
					this.balancear(bisavo, avo, pai, i);
				}
			}

			if (m.getID() < i.municipio.getID()) {
				this.inserir(m, avo, pai, i, i.esq);
			} else if (m.getID() > i.municipio.getID()) {
				this.inserir(m, avo, pai, i, i.dir);
			}
		}
	}

	private void balancear(No bisavo, No avo, No pai, No i) {
		if (pai.cor == true) {
			if (pai.municipio.getID() > avo.municipio.getID()) {
				if (i.municipio.getID() > pai.municipio.getID()) {
					avo = this.rotacaoEsq(avo);
				} else {
					avo = rotacaoDirEsq(avo);
				}
			} else {
				if (i.municipio.getID() < pai.municipio.getID()) {
					avo = this.rotacaoDir(avo);
				} else {
					avo = this.rotacaoEsqDir(avo);
				}
			}

			if (bisavo == null) {
				raiz = avo;
			} else {
				if (avo.municipio.getID() < bisavo.municipio.getID()) {
					bisavo.esq = avo;
				} else {
					bisavo.dir = avo;
				}
			}

			avo.cor = false;
			avo.esq.cor = avo.dir.cor = true;
		}
	}

	private No rotacaoDir(No no) {
		No noEsq = no.esq;
		No noEsqDir = noEsq.dir;
		
		noEsq.dir = no;
		no.esq = noEsqDir;

		return noEsq;
	}

	private No rotacaoEsq(No no) {
		No noDir = no.dir;
		No noDirEsq = noDir.esq;

		noDir.esq = no;
		no.dir = noDirEsq;

		return noDir;
	}

	private No rotacaoDirEsq(No no) {
		no.dir = this.rotacaoDir(no.dir);
		return this.rotacaoEsq(no);
	}

	private No rotacaoEsqDir(No no) {
		no.esq = this.rotacaoEsq(no.esq);
		return this.rotacaoDir(no);
	}

	/**
	 * Verifica a existencia de um municipio com o id indicado por parametro.
	 * @param id ID do municipio a ser pesquisado.
	 * @return <code>true</code> se o municipio existir, <code>false</code> caso contrario.
	 */
	public boolean pesquisar(int id) {
		MyIO.print("raiz ");
		return pesquisar(id, raiz);
	}

	private boolean pesquisar(int id, No i) {
		boolean resp = false;
		
		if (i == null) {
			resp = false;
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (id == i.municipio.getID()) {
			resp = true;
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (id < i.municipio.getID()) {
			MyIO.print("esq ");
			resp = this.pesquisar(id, i.esq);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (id > i.municipio.getID()) {
			MyIO.print("dir ");
			resp = this.pesquisar(id, i.dir);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		}

		return resp;
	}

	public void mostrar() {
		this.mostrar(raiz);
	}

	private void mostrar(No i) {
		if (i != null) {
			this.mostrar(i.esq);
			i.getMunicipio().imprimir();
			this.mostrar(i.dir);
		}
	}
}

class Municipio {

	//Atributos
	private int id;
	private String nome;
	private String UF;
	private int codigoUF;
	private int populacao;
	private int numFuncionarios;
	private int numComissionados;
	private String escolaridade;
	private String estagio;
	private int attPlano;
	private String regiao;
	private int attCadastro;
	private boolean isConsorcio;

	Municipio() {
		this(0, "", "", 0, 0, 0, 0, "", "", 0, "", 0, false);
	}

	Municipio(int id, String nome, String UF, int codigoUF, int populacao, int numFuncionarios, int numComissionados, String escolaridade, String estagio, int attPlano, String regiao, int attCadastro, boolean isConsorcio) {

		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);		
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setIsConsorcio(isConsorcio);
	}

	//Getters e Setters

	public void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return this.id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}

	public void setUF(String UF) {
		this.UF = UF;
	}

	public String getUF() {
		return this.UF;
	}

	public void setCodigoUF(int codigoUF) {
		this.codigoUF = codigoUF;
	}

	public int getCodigoUF() {
		return this.codigoUF;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public int getPopulacao() {
		return this.populacao;
	}

	public void setNumFuncionarios(int numFuncionarios) {
		this.numFuncionarios = numFuncionarios;
	}

	public int getNumFuncionarios() {
		return this.numFuncionarios;
	}

	public void setNumComissionados(int numComissionados) {
		this.numComissionados = numComissionados;
	}

	public int getNumComissionados() {
		return this.numComissionados;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getEscolaridade() {
		return this.escolaridade;
	}

	public void setEstagio(String estagio) {
		this.estagio = estagio;
	}

	public String getEstagio() {
		return this.estagio;
	}

	public void setAttPlano(int attPlano) {
		this.attPlano = attPlano;
	}

	public int getAttPlano() {
		return this.attPlano;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getRegiao() {
		return this.regiao;
	}

	public void setAttCadastro(int attCadastro) {
		this.attCadastro = attCadastro;
	}

	public int getAttCadastro() {
		return this.attCadastro;
	}

	public void setIsConsorcio(boolean isConsorcio) {
		this.isConsorcio = isConsorcio;
	}

	public boolean getIsConsorcio() {
		return this.isConsorcio;
	}

	public Municipio clone() {
		Municipio clone = new Municipio();

		clone.setID(this.getID());
		clone.setNome(this.getNome());
		clone.setUF(this.getUF());
		clone.setCodigoUF(this.getCodigoUF());
		clone.setPopulacao(this.getPopulacao());
		clone.setNumFuncionarios(this.getNumFuncionarios());
		clone.setNumComissionados(this.getNumComissionados());
		clone.setEscolaridade(this.getEscolaridade());
		clone.setEstagio(this.getEstagio());
		clone.setAttPlano(this.getAttPlano());
		clone.setRegiao(this.getRegiao());
		clone.setAttCadastro(this.getAttCadastro());
		clone.setIsConsorcio(this.getIsConsorcio());

		return clone;
	}

	public void imprimir() {
		MyIO.println(
			this.getID() + " " +
			this.getNome() + " " +
			this.getUF() + " " +
			this.getCodigoUF() + " " +
			this.getPopulacao() + " " +
			this.getNumFuncionarios() + " " +
			this.getNumComissionados() + " " +
			this.getEscolaridade() + " " +
			this.getEstagio() + " " +
			this.getAttPlano() + " " +
			this.getRegiao() +  " " +
			this.getAttCadastro() + " " +
			this.getIsConsorcio() + ""
		);
	}

	public void ler(int registro) {
		String charset = "ISO-8859-1"; //ISO-8859-1
		int i = 0;
		String linha;

		//Arquivo 1
		String arq = "/tmp/articulacaoointerinstitucional.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq1Data = linha.split("\t");

		//Variaveis a serem extraidas do arquivo
		int id = (this.isInteger(arq1Data[0])) ? Integer.parseInt(arq1Data[0]) : 0;
		int codigoUF = Integer.parseInt(arq1Data[1]);
		String nome = arq1Data[3];

		boolean isConsorcio = (arq1Data[5].equals("Sim")) ? true : false;

		Arq.close();

		//Arquivo 2
		arq = "/tmp/gestaoambiental.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq2Data = linha.split("\t");
		String estagio = arq2Data[7];

		Arq.close();

		//Arquivo 3
		arq = "/tmp/planejamentourbano.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq3Data = linha.split("\t");

		String escolaridade = arq3Data[5];
		int attPlano = (this.isInteger(arq3Data[8])) ? Integer.parseInt(arq3Data[8]) : 0;

		Arq.close();

		//Arquivo 4
		arq = "/tmp/recursoshumanos.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq4Data = linha.split("\t");

		int numFuncionarios = this.isInteger(arq4Data[7]) ? Integer.parseInt(arq4Data[4]) : 0;
		int numComissionados = (this.isInteger(arq4Data[7])) ? Integer.parseInt(arq4Data[7]) : 0;

		Arq.close();

		//Arquivo 5
		arq = "/tmp/recursosparagestao.txt";
		Arq.openRead(arq, charset);
		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq5Data = linha.split("\t");

		int attCadastro = this.isInteger(arq5Data[6]) ? Integer.parseInt(arq5Data[6]) : 0;

		Arq.close();

		//Arquivo 6
		arq = "/tmp/terceirizacaoeinformatizacao.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq6Data = linha.split("\t");

		Arq.close();

		//Arquivo 7
		arq = "/tmp/variaveisexternas.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq7Data = linha.split("\t");

		String regiao = arq7Data[1];
		int populacao = this.isInteger(arq7Data[6]) ? Integer.parseInt(arq7Data[6]) : 0;
		String UF = arq7Data[3];

		Arq.close();

		//Setando os atributos
		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setRegiao(regiao);
		this.setAttCadastro(attCadastro);
		this.setIsConsorcio(isConsorcio);
	}

	public boolean isInteger(String p) {
		boolean isInt = false;
		try {
			int number = Integer.parseInt(p);

			isInt = true;
		} catch (Exception e) {
			isInt = false;
		}

		return isInt;
	}

	public boolean equals(String s1, String s2) {
		boolean equals = false;		
		
		if (s1.length() == s2.length()) {
			for (int i = 0; i < s1.length(); i++) {
				MyIO.println(s1.charAt(i) + " " + s2.charAt(i));
			}
		}

		return equals;
	}
}

public class Questao4 {
	public static void main(String[] args) throws Exception {

		ArvoreAlvinegra avn = new ArvoreAlvinegra();
		MyIO.setCharset("ISO-8859-1");

		for (int linha = MyIO.readInt(); linha != 0; linha = MyIO.readInt()) {
			Municipio m = new Municipio();
			m.ler(linha);
			avn.inserir(m);
		}

		int contador = MyIO.readInt();
		
		long inicio = System.currentTimeMillis();
			
		for (int i = 0; i < contador; i++) {
			String comando = MyIO.readString();
			Municipio mu = new Municipio();

			if (comando.charAt(0) == 'I') {
				int registro = MyIO.readInt();
				mu.ler(registro);
				avn.inserir(mu);
			} else if (comando.charAt(0) == 'R') {
				int id = MyIO.readInt();
				//avn.remover(id);
			}
		}
		
		for (String linha1 = MyIO.readLine(); linha1.equals("FIM") == false; linha1 = MyIO.readLine()) {
			int id = Integer.parseInt(linha1);

			boolean exists = avn.pesquisar(id);
			String saida = exists ? "SIM" : "NAO";
			MyIO.println(saida);
		}

		long fim = System.currentTimeMillis();
		long diff = (fim - inicio) / 1000;

		writeLog(diff, avn.getNumComparacoes());
	}

	public static void writeLog(long time, int comparacoes) {
		Arq.openWrite("matricula_alvinegra.txt");
		Arq.print("561325\t");
		Arq.print(time + "\t" + comparacoes);
		Arq.close();
	}
}
