import java.time.LocalDateTime;
import java.util.*;

class TP03Q02ListaSequencial {
   public static void main (String args[]) throws Exception {
      Lista lista = new Lista(1000);
      MyIO.setCharset("ISO-8859-1");

      for(int linha = MyIO.readInt(); linha > 0; linha = MyIO.readInt()){
         Municipio municipio = new Municipio();
         municipio.ler(linha);
         lista.inserirFim(municipio);
      }

      int n = MyIO.readInt();

      for(int i = 0; i < n; i++){
         String comando = MyIO.readString();
         Municipio municipio = new Municipio();
         if(comando.charAt(0) == 'I'){
            if(comando.charAt(1) == 'I'){
               municipio.ler(MyIO.readInt());
               lista.inserirInicio(municipio);
            } else if (comando.charAt(1) == 'F'){
               municipio.ler(MyIO.readInt());
               lista.inserirFim(municipio);
            } else if (comando.charAt(1) == '*'){
               int pos = MyIO.readInt();
               municipio.ler(MyIO.readInt());
               lista.inserir(municipio, pos);
            } else {
               MyIO.println("Comando invalido: " + comando);
               System.exit(1);
            }
         } else if (comando.charAt(0) == 'R'){
            if(comando.charAt(1) == 'I'){
               municipio = lista.removerInicio();
            } else if (comando.charAt(1) == 'F'){
               municipio = lista.removerFim();
            } else if (comando.charAt(1) == '*'){
               municipio = lista.remover(MyIO.readInt());
            } else {
               MyIO.println("Comando invalido: " + comando);
               System.exit(1);
            }
            MyIO.println("(R) " + municipio.nome);
         } else {
            MyIO.println("Comando invalido: " + comando);
            System.exit(1);
         }
      }
      lista.imprimir();
   }
}
