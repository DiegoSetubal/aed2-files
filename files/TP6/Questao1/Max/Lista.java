class Lista {
   private Municipio[] array;
   private int n;


   public Lista () {
      this(6);
   }


   public Lista (int tamanho){
      array = new Municipio[tamanho];
      n = 0;
   }


   public void inserirInicio(Municipio x) throws Exception {

      //validar insercao
      if(n >= array.length){
         throw new Exception("Erro ao inserir!");
      } 

      //levar elementos para o fim do array
      for(int i = n; i > 0; i--){
         array[i] = array[i-1].clone();
      }

      array[0] = x.clone();
      n++;
   }


   public void inserirFim(Municipio x) throws Exception {

      //validar insercao
      if(n >= array.length){
         throw new Exception("Erro ao inserir!");
      }
      array[n] = x.clone();
      n++;
   }


   public void inserir(Municipio x, int pos) throws Exception {

      //validar insercao
      if(n >= array.length || pos < 0 || pos > n){
         throw new Exception("Erro ao inserir!");
      }

      //levar elementos para o fim do array
      for(int i = n; i > pos; i--){
         array[i] = array[i-1].clone();
      }

      array[pos] = x.clone();
      n++;
   }


   public Municipio removerInicio() throws Exception {

      //validar remocao
      if (n == 0) {
         throw new Exception("Erro ao remover!");
      }

      Municipio resp = array[0].clone();
      n--;

      for(int i = 0; i < n; i++){
         array[i] = array[i+1].clone();
      }

      return resp;
   }


   public Municipio removerFim() throws Exception {

      //validar remocao
      if (n == 0) {
         throw new Exception("Erro ao remover!");
      }

      return array[--n].clone();
   }


   public Municipio remover(int pos) throws Exception {

      //validar remocao
      if (n == 0 || pos < 0 || pos >= n) {
         throw new Exception("Erro ao remover!");
      }

      Municipio resp = array[pos].clone();
      n--;

      for(int i = pos; i < n; i++){
         array[i] = array[i+1].clone();
      }

      return resp;
   }


   public void imprimir (){
      for(int i = 0; i < n; i++){
         array[i].imprimir();
      }
   }

   public void imprimir (int i){
      array[i].imprimir();
   }

   public int quantos (){
      return n;
   }

   public double getMedia(){
      double resp = 0;
      for(int i = 0; i < n; i++){
         resp += array[i].populacao;
      }
      return resp /= n;
   }
}
