class Municipio implements Comparable<Municipio> {
   public int id, codigouf, populacao, numeroFuncionarios, numeroComissionados, atualizacaoPlano, atualizacaoCadastro;
   public String nome, uf, escolaridade, estagio, regiao;
   public boolean consorcio;

   public static int ordenacao = 0;
   public static final int SELECAO = 1;
   public static final int INSERCAO = 2;
   public static final int SHELLSORT = 3;
   public static final int QUICKSORT = 4;
   public static final int HEAPSORT = 5;
   public static final int COUNTING = 6;
   public static final int BOLHA = 7;
   public static final int MERGESORT = 8;
   public static final int RADIXSORT = 9;


   public int compareTo(Municipio outro) {
      int resp = 0;

      if(ordenacao == SELECAO){
         resp = (atualizacaoPlano == outro.atualizacaoPlano) ? 0 : ((atualizacaoPlano > outro.atualizacaoPlano) ? 1 : -1);
      } else if(ordenacao == INSERCAO){
         resp = nome.compareTo(outro.nome);
      } else if(ordenacao == SHELLSORT){
         resp = uf.compareTo(outro.uf);
      } else if(ordenacao == HEAPSORT){
         resp = (codigouf == outro.codigouf) ? 0 : ((codigouf > outro.codigouf) ? 1 : -1);
      } else if(ordenacao == QUICKSORT){
         resp = (populacao == outro.populacao) ? 0 : ((populacao > outro.populacao) ? 1 : -1);         
      } else if(ordenacao == COUNTING){
         resp = (numeroFuncionarios == outro.numeroFuncionarios) ? 0 : ((numeroFuncionarios > outro.numeroFuncionarios) ? 1 : -1);         
      } else if(ordenacao == BOLHA){
         resp = escolaridade.compareTo(outro.escolaridade);
      } else if(ordenacao == MERGESORT){
         resp = (numeroComissionados == outro.numeroComissionados) ? 0 : ((numeroComissionados > outro.numeroComissionados) ? 1 : -1);         
      } else if(ordenacao == RADIXSORT){
         int doidao = atualizacaoPlano * 1000 + id;
         int outroDoidao = outro.atualizacaoPlano * 1000 + outro.id;
         resp = (doidao == outroDoidao) ? 0 : ((doidao > outroDoidao) ? 1 : -1);         
      }

      if(resp == 0){
         resp = (id == outro.id) ? 0 : ((id > outro.id) ? 1 : -1);         
      }

      return resp;
   }

   public void imprimir(){

         MyIO.println(this.id + " " + 
                      this.nome + " " + 
                      this.uf + " " + 
                      this.codigouf + " " + 
                      this.populacao + " " + 
                      this.numeroFuncionarios + " " + 
                      this.numeroComissionados + " " + 
                      this.escolaridade + " " + 
                      this.estagio + " " + 
                      this.atualizacaoPlano + " " + 
                      this.regiao + " " + 
                      this.atualizacaoCadastro + " " + 
                      this.consorcio);
   }

   public Municipio clone (){
      Municipio resp = new Municipio();
      resp.id = this.id;
      resp.nome = this.nome;
      resp.uf = this.uf;
      resp.codigouf = this.codigouf;
      resp.populacao = this.populacao;
      resp.numeroFuncionarios = this.numeroFuncionarios;
      resp.numeroComissionados = this.numeroComissionados;
      resp.escolaridade = this.escolaridade;
      resp.estagio = this.estagio;
      resp.atualizacaoPlano = this.atualizacaoPlano;
      resp.regiao = this.regiao;
      resp.atualizacaoCadastro = this.atualizacaoCadastro;
      resp.consorcio = this.consorcio;
      return resp;
   }

   public boolean isDigito(char c){
      return (c >= '0' && c <= '9');
   }

   public boolean isNumero(String s) {
      boolean resp = true;

      for(int i = 0; i < s.length(); i++){
         if (isDigito(s.charAt(i)) == false){
            resp = false;
            i = s.length();
         }
      }

      return resp;
   }


   public void ler(int linha) throws Exception {
      String terceirizacaoeinformatizacao, articulacaoointerinstitucional, gestaoambiental, planejamentourbano, recursoshumanos, recursosparagestao, variaveisexternas;
      String array[];

      Arq.openRead("/tmp/recursoshumanos.txt", "ISO-8859-1");
      for(int i = 0; i < linha; Arq.readLine(), i++);
      recursoshumanos = Arq.readLine();
      array = recursoshumanos.split("\t");
      this.id = (isNumero(array[0])) ? new Integer(array[0]).intValue() : 0;       //int Recursos Humanos
      this.numeroFuncionarios = (isNumero(array[4])) ? new Integer(array[4]).intValue() : 0;   //int Recursos Humanos
      this.numeroComissionados = (isNumero(array[7])) ? new Integer(array[7]).intValue() : 0;  //int Recursos Humanos
      //MyIO.println("Recursos - " + array[0] + " --- " + array[4] + " --- " + array[7]);
      Arq.close();

      Arq.openRead("/tmp/planejamentourbano.txt", "ISO-8859-1");
      for(int i = 0; i < linha; Arq.readLine(), i++);
      planejamentourbano = Arq.readLine();
      array = planejamentourbano.split("\t");
      this.escolaridade = array[5];    //String Planejamento Urbano
      this.atualizacaoPlano = (isNumero(array[8])) ? new Integer(array[8]).intValue() : 0;  //int Planejamento Urbano
      //MyIO.println("Plan Urb - " + array[5] + " --- " + array[8]);
      Arq.close();

      Arq.openRead("/tmp/recursosparagestao.txt", "ISO-8859-1");
      for(int i = 0; i < linha; Arq.readLine(), i++);
      recursosparagestao = Arq.readLine();
      array = recursosparagestao.split("\t");
      this.atualizacaoCadastro = (isNumero(array[6])) ? new Integer(array[6]).intValue() : 0;  //int Recursos para Gestão Municipal
      //MyIO.println("Rec para Ges - " + array[6]);
      Arq.close();

      Arq.openRead("/tmp/gestaoambiental.txt", "ISO-8859-1");
      for(int i = 0; i < linha; Arq.readLine(), i++);
      gestaoambiental = Arq.readLine();
      array = gestaoambiental.split("\t");
      this.estagio = array[7];      //String Gestão Ambiental
      //MyIO.println("Ges Am - " + array[7]);
      Arq.close();

      Arq.openRead("/tmp/articulacaoointerinstitucional.txt", "ISO-8859-1");
      for(int i = 0; i < linha; Arq.readLine(), i++);
      articulacaoointerinstitucional = Arq.readLine();
      array = articulacaoointerinstitucional.split("\t");
      this.consorcio = array[5].equals("Sim");    //boolean   Articulacao Interinstitucional
      //MyIO.println("Art In - " + array[5]);
      Arq.close();

      Arq.openRead("/tmp/variaveisexternas.txt", "ISO-8859-1");
      for(int i = 0; i < linha; Arq.readLine(), i++);
      variaveisexternas = Arq.readLine();
      array = variaveisexternas.split("\t");
      this.regiao = array[1];       //String Variaveis Externas
      this.codigouf = (isNumero(array[2])) ? new Integer(array[2]).intValue() : 0;    //int Variaveis Externas
      this.uf = array[3];        //String Variaveis Externas
      this.nome = array[4];         //String Variaveis Externas
      this.populacao = (isNumero(array[6])) ? new Integer(array[6]).intValue() : 0;      //int Variaveis Externas
      //MyIO.println("Var Ex - " + array[1] + " --- " + array[2] + " --- " + array[3] + " --- " + array[4] + " --- " + array[6]);
      Arq.close();
      //System.out.println(variaveisexternas);
   }


}
