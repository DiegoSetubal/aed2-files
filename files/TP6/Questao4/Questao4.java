/**
 * Classe lista com alocação dinâmica, com celulas.
 * @author Diego Oliveira
 */
class Celula {
	private Municipio municipio;
	public Celula ant;
	public Celula prox;

	Celula() {
		this.prox = this.ant = null;
		this.setMunicipio(new Municipio());
	}
	
	Celula(Municipio m) {
		this.prox = this.ant = null;
		this.setMunicipio(m);
	}

	public void setMunicipio(Municipio m) {
		this.municipio = m.clone();
	}

	public Municipio getMunicipio() {
		return this.municipio;
	}
}

class Lista {
	private Celula primeiro;
	private Celula ultimo;
	
	private int numComparacoes;
	private int numMovimentacoes;

	/**
	 * Construtor da classe.
	 */
	public Lista() {
		this.primeiro = new Celula();
		this.ultimo = primeiro;
		
		this.setNumMovimentacoes(0);
		this.setNumComparacoes(0);
	}

	
	private void setNumMovimentacoes(int n) {
        this.numMovimentacoes = n;
    }

    public int getNumMovimentacoes() {
        return this.numMovimentacoes;
    }

    private void setNumComparacoes(int n) {
        this.numComparacoes = n;
    }

    public int getNumComparacoes() {
        return this.numComparacoes;
    }
	
	/**
	 * Insere um elemento no início da lista.
	 * @param m municipio a ser inserido na lista.
	 */
	public void inserirInicio(Municipio m) {
		Celula tmp = new Celula(m);

		tmp.ant = primeiro;
		tmp.prox = primeiro.prox;
		primeiro.prox = tmp;
		
		if (this.isVazia()) {
			ultimo = tmp;
		} else {
			tmp.prox.ant = tmp;
		}
		
		tmp = null;
	}

	/**
	 * Insere um elemento no final da lista.
	 * @param m municipio a ser inserido na lista.
	 */
	public void inserirFim(Municipio m) {
		ultimo.prox = new Celula(m);
		ultimo.prox.ant = ultimo;
		ultimo = ultimo.prox;
	}
	
	/**
	 * Remove um elemento do começo da lista.
	 * @return Municipio a ser removido da lista.
	 * @throws Exception se a lista estiver vazia.
	 */
	public Municipio removerInicio() throws Exception {
		if (this.isVazia()) {
			throw new Exception("Erro ao remover.");
		}
		
		Celula tmp = primeiro;
		primeiro = primeiro.prox;

		Municipio resp = primeiro.getMunicipio().clone();
		tmp.prox = null;
		tmp = null;

		return resp;
	}

	/**
	 * Remove um elemento do final da lista.
	 * @return Municipio a ser removido.
	 * @throws Exception se a lista estiver vazia.
	 */
	public Municipio removerFim() throws Exception {
		if (this.isVazia()) {
			throw new Exception("Erro ao remover.");
		}

		Municipio resp = ultimo.getMunicipio().clone();
		ultimo = ultimo.ant;
		ultimo.prox.ant = null;
		ultimo.prox = null;

		return resp;
	}

	/**
     * Insere um elemento em uma posicao especifica considerando que o 
     * primeiro elemento valido esta na posicao 0.
     * @param m Municipio a ser inserido.
     * @param pos int posição onde o elemento será  inserido.
     * @throws Exception Se <code>pos</code> invalida.
     */
	public void inserir(Municipio m, int pos) throws Exception {
		
		int tamanho = this.getTamanho();

		if (pos < 0 || pos > tamanho) {
			throw new Exception("Posição inválida.");
		} else if (pos == 0) {
			this.inserirInicio(m);
		} else if (pos == tamanho) {
			this.inserirFim(m);
		} else {
			Celula i = primeiro;
			for (int j = 0; j < pos; j++, i = i.prox);

			Celula tmp = new Celula(m);
			tmp.ant = i;
			tmp.prox = i.prox;
			tmp.ant.prox = tmp.prox.ant = tmp;
			tmp = i = null;
		}
	}

	/**
     * Remove um elemento de uma posição específica da lista
	 * considerando que a primeira posição válida é 0.
     * @param pos Posição a ser removida
     * @return resp int elemento a ser removido.
     * @throws Exception Se <code>posicao</code> invalida.
     */
	public Municipio remover(int pos) throws Exception {
		Municipio resp;
		int tamanho = this.getTamanho();

		if (this.isVazia()){
			throw new Exception("Erro ao remover (vazia)!");
		} else if(pos < 0 || pos >= tamanho){
			throw new Exception("Erro ao remover (posicao " + pos + " / " + tamanho + " invalida!");
		} else if (pos == 0){
			resp = this.removerInicio();
		} else if (pos == tamanho - 1){
			resp = this.removerFim();
		} else {
			Celula i = primeiro.prox;
			for(int j = 0; j < pos; j++, i = i.prox);

			i.ant.prox = i.prox;
			i.prox.ant = i.ant;
			
			resp = i.getMunicipio().clone();
			
			i.prox = i.ant = null;
			i = null;
		}

		return resp;
	}

	/**
	 * Verifica a existência de um municipio com o id indicado por parâmetro.
	 * @param id ID do município a ser pesquisado.
	 * @return <code>true</code> se o municipio existir, <code>false</code> caso contrário.
	 */
	public boolean pesquisar(int id) {
		boolean exists = false;

		for (Celula i = primeiro.prox; i != null && (exists == false); i = i.prox) {
			Municipio m = i.getMunicipio();
			exists = (m.getID() == id);
		}

		return exists;
	}

	public void mostrar() {
		for (Celula i = primeiro.prox; i != null; i = i.prox) {
			i.getMunicipio().imprimir();
		}
	}

	public int getTamanho() {
		int tamanho = 0;
		for (Celula tmp = primeiro; tmp != ultimo; tmp = tmp.prox, tamanho++);
		
		return tamanho;
	}

	public boolean isVazia() {
		return (primeiro == ultimo);
	}

	public void quicksort() {
		this.quicksort(primeiro.prox, ultimo);
	}

	private void quicksort(Celula esq, Celula dir) {
		if (esq != dir.prox) {

			Celula pivo = esq;
			Celula i = esq.prox;
			Celula j = dir;

			while (i.getMunicipio().getPopulacao() < j.getMunicipio().getPopulacao()) {
				if (esq.getMunicipio().getPopulacao() > pivo.getMunicipio().getPopulacao()) {
					dir = dir.ant;
					swap(esq, dir);
				} else {
					esq = esq.ant;
				}

				this.setNumComparacoes(this.getNumComparacoes() + 2);
			}
			
			swap(esq, i);
			quicksort(esq, j);
			quicksort(i, dir);
		}
	}

	private void swap(Celula i, Celula j) {
		Municipio aux = i.getMunicipio().clone();

		i.setMunicipio(j.getMunicipio().clone());
		j.setMunicipio(aux.clone());
		
		this.setNumMovimentacoes(this.getNumMovimentacoes() + 3);
	}
	
}

class Municipio {

	//Atributos
	private int id;
	private String nome;
	private String UF;
	private int codigoUF;
	private int populacao;
	private int numFuncionarios;
	private int numComissionados;
	private String escolaridade;
	private String estagio;
	private int attPlano;
	private String regiao;
	private int attCadastro;
	private boolean isConsorcio;

	Municipio() {
		this(0, "", "", 0, 0, 0, 0, "", "", 0, "", 0, false);
	}

	Municipio(int id, String nome, String UF, int codigoUF, int populacao, int numFuncionarios, int numComissionados, String escolaridade, String estagio, int attPlano, String regiao, int attCadastro, boolean isConsorcio) {

		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);		
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setIsConsorcio(isConsorcio);
	}

	//Getters e Setters

	public void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return this.id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}

	public void setUF(String UF) {
		this.UF = UF;
	}

	public String getUF() {
		return this.UF;
	}

	public void setCodigoUF(int codigoUF) {
		this.codigoUF = codigoUF;
	}

	public int getCodigoUF() {
		return this.codigoUF;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public int getPopulacao() {
		return this.populacao;
	}

	public void setNumFuncionarios(int numFuncionarios) {
		this.numFuncionarios = numFuncionarios;
	}

	public int getNumFuncionarios() {
		return this.numFuncionarios;
	}

	public void setNumComissionados(int numComissionados) {
		this.numComissionados = numComissionados;
	}

	public int getNumComissionados() {
		return this.numComissionados;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getEscolaridade() {
		return this.escolaridade;
	}

	public void setEstagio(String estagio) {
		this.estagio = estagio;
	}

	public String getEstagio() {
		return this.estagio;
	}

	public void setAttPlano(int attPlano) {
		this.attPlano = attPlano;
	}

	public int getAttPlano() {
		return this.attPlano;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getRegiao() {
		return this.regiao;
	}

	public void setAttCadastro(int attCadastro) {
		this.attCadastro = attCadastro;
	}

	public int getAttCadastro() {
		return this.attCadastro;
	}

	public void setIsConsorcio(boolean isConsorcio) {
		this.isConsorcio = isConsorcio;
	}

	public boolean getIsConsorcio() {
		return this.isConsorcio;
	}

	public Municipio clone() {
		Municipio clone = new Municipio();

		clone.setID(this.getID());
		clone.setNome(this.getNome());
		clone.setUF(this.getUF());
		clone.setCodigoUF(this.getCodigoUF());
		clone.setPopulacao(this.getPopulacao());
		clone.setNumFuncionarios(this.getNumFuncionarios());
		clone.setNumComissionados(this.getNumComissionados());
		clone.setEscolaridade(this.getEscolaridade());
		clone.setEstagio(this.getEstagio());
		clone.setAttPlano(this.getAttPlano());
		clone.setRegiao(this.getRegiao());
		clone.setAttCadastro(this.getAttCadastro());
		clone.setIsConsorcio(this.getIsConsorcio());

		return clone;
	}

	public void imprimir() {
		MyIO.println(
			this.getID() + " " +
			this.getNome() + " " +
			this.getUF() + " " +
			this.getCodigoUF() + " " +
			this.getPopulacao() + " " +
			this.getNumFuncionarios() + " " +
			this.getNumComissionados() + " " +
			this.getEscolaridade() + " " +
			this.getEstagio() + " " +
			this.getAttPlano() + " " +
			this.getRegiao() +  " " +
			this.getAttCadastro() + " " +
			this.getIsConsorcio() + ""
		);
	}

	public void ler(int registro) {
		String charset = "ISO-8859-1"; //ISO-8859-1
		int i = 0;
		String linha;

		//Arquivo 1
		String arq = "/tmp/articulacaoointerinstitucional.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq1Data = linha.split("\t");

		//Variáveis a serem extraídas do arquivo
		int id = (this.isInteger(arq1Data[0])) ? Integer.parseInt(arq1Data[0]) : 0;
		int codigoUF = Integer.parseInt(arq1Data[1]);
		String nome = arq1Data[3];

		/////////////DEBUGAR ESTA JOÇA!

		boolean isConsorcio = (arq1Data[5].equals("Sim")) ? true : false;

		Arq.close();

		//Arquivo 2
		arq = "/tmp/gestaoambiental.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq2Data = linha.split("\t");
		String estagio = arq2Data[7];

		Arq.close();

		//Arquivo 3
		arq = "/tmp/planejamentourbano.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq3Data = linha.split("\t");

		String escolaridade = arq3Data[5];
		int attPlano = (this.isInteger(arq3Data[8])) ? Integer.parseInt(arq3Data[8]) : 0;

		Arq.close();

		//Arquivo 4
		arq = "/tmp/recursoshumanos.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq4Data = linha.split("\t");

		int numFuncionarios = this.isInteger(arq4Data[7]) ? Integer.parseInt(arq4Data[4]) : 0;
		int numComissionados = (this.isInteger(arq4Data[7])) ? Integer.parseInt(arq4Data[7]) : 0;

		Arq.close();

		//Arquivo 5
		arq = "/tmp/recursosparagestao.txt";
		Arq.openRead(arq, charset);
		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq5Data = linha.split("\t");

		int attCadastro = this.isInteger(arq5Data[6]) ? Integer.parseInt(arq5Data[6]) : 0;

		Arq.close();

		//Arquivo 6
		arq = "/tmp/terceirizacaoeinformatizacao.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq6Data = linha.split("\t");

		Arq.close();

		//Arquivo 7
		arq = "/tmp/variaveisexternas.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq7Data = linha.split("\t");

		String regiao = arq7Data[1];
		int populacao = this.isInteger(arq7Data[6]) ? Integer.parseInt(arq7Data[6]) : 0;
		String UF = arq7Data[3];

		Arq.close();

		//Setando os atributos
		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setRegiao(regiao);
		this.setAttCadastro(attCadastro);
		this.setIsConsorcio(isConsorcio);
	}

	public boolean isInteger(String p) {
		boolean isInt = false;
		try {
			int number = Integer.parseInt(p);

			isInt = true;
		} catch (Exception e) {
			isInt = false;
		}

		return isInt;
	}

	public boolean equals(String s1, String s2) {
		boolean equals = false;		
		
		if (s1.length() == s2.length()) {
			for (int i = 0; i < s1.length(); i++) {
				MyIO.println(s1.charAt(i) + " " + s2.charAt(i));
			}
		}

		return equals;
	}
}

public class Questao4 {
	public static void main(String[] args) throws Exception {
		Lista lista = new Lista();
		MyIO.setCharset("ISO-8859-1");
		
		for (int linha = MyIO.readInt(); linha != 0; linha = MyIO.readInt()) {
            Municipio m = new Municipio();
            m.ler(linha);
            lista.inserirFim(m);
        }
		
		long inicio = System.currentTimeMillis();
        //lista.quicksort();
        long fim = System.currentTimeMillis();
        long diff = (fim - inicio) / 1000;

        lista.mostrar();
        int numComparacoes = lista.getNumComparacoes();
        int numMovimentacoes = lista.getNumMovimentacoes();
        writeLog(diff, numComparacoes, numMovimentacoes);
	}
	
	public static void writeLog(long runningTime, int numComparacoes, int numMovimentacoes) {
        Arq.openWrite("matricula_quicksort2.txt");
        Arq.print("561325\t");
        Arq.print(runningTime + "\t" + numComparacoes + "\t" + numMovimentacoes);
        Arq.close();
    }
	
}
